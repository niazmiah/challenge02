// userReg.js
// This script manages user registration.

//user array
var user = {username: " ", 
            name: " ", 
            email: " ", 
            password: " ", 
            age: 0}, id, username, name, email, password, age, userarray = [];

function User(id, username, name, email, password, age) {
    'use strict';
    this.id = id;
    this.username = username;
    this.name = name;
    this.email = email;
    this.password = password;
    this.age = age;
    
    //creates a new row
    this.createrow = function () {
        var row = "<tr id='" + this.id + "'>" + 
            "<td>" + this.id + "</td>" + 
            "<td>" + this.username + "</td>" + 
            "<td>" + this.name + "</td>" + 
            "<td>" + this.email + "</td>" + 
            "<td>" + this.password + "</td>" + 
            "<td>" + this.age + "</td></tr>";
        return row;
    };
}

function userReg() {
    'use strict';
    id = userarray.length + 1
    username = document.getElementById("username").value;
    name = document.getElementById("name").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    age = document.getElementById("age").value;
    
    for (var i = 0; i < userarray.length; i++) {
        if (username === userarray[i].username) {
            alert("Sorry, that username already exists, try again.");
            return false;
        }
    }
    
    userarray[userarray.length] = new User(id, username, name, email, password, parseInt(age));
    

    document.getElementById('users').innerHTML = 
        document.getElementById('users').innerHTML + userarray[userarray.length - 1].createrow();
        
}

function validateForm() {
   //validation
    var x = document.forms["myForm"]["username"].value;
    if (x==null || x=="") {
        alert("Username must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["name"].value;
    if (x==null || x=="") {
        alert("Name must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
        return false;
    }
    var x = document.forms["myForm"]["password"].value;
    if (x==null || x=="") {
        alert("Password must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["age"].value;
    if (x==null || x=="") {
        alert("Age must be filled out");
        return false;
    }
    return userReg();
}

function averageAge() {
    'use strict';
    var button = document.getElementById("avgAge");
    button.addEventListener("click", function () {
        var avgAge = 0, i;
        for (i = 0; i < userarray.length; i++) {
            avgAge = avgAge + userarray[i].age;
        }
        avgAge = avgAge / userarray.length;
        alert('The average age of registered users is: ' + avgAge);
    });
}

window.onload = averageAge;